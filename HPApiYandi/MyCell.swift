//
//  MyCell.swift
//  HarryPotterApiYandi
//
//  Created by COTEMIG on 09/04/44 AH.
//

import UIKit

class MyCell: UITableViewCell {

    @IBOutlet weak var imgPersonagem: UIImageView!
    @IBOutlet weak var nomePersonagem: UILabel!
    @IBOutlet weak var nomeAutor: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
